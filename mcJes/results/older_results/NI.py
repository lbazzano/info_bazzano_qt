import numpy as np
from scipy.stats import norm
import matplotlib.pyplot as plt
from random import random
from random import choices
import math

N = 500
eff = 0.6
err_eff = 0.1
low = 200000
high = 300000


Scale = (high-low) * err_eff
Xaxis = np.linspace(low,high,101)
Yaxis = np.power(Xaxis,-4.0)
Yweights = Yaxis/np.sum(Yaxis)
X_true = np.array([choices(Xaxis,Yweights)[0] for n in range(N)])

#X_true = np.random.uniform(low = 100,high = 200, size=N)
rand   = np.random.normal(loc = 0,scale=Scale, size=N)
X_reco = X_true * eff + rand
plt.hist(X_true,label='true X',alpha=0.5)
plt.hist(X_reco,label='reco X',alpha=0.5)
plt.legend()
plt.xlabel('X (pt/m/E)')
plt.xlim(0,high*1.1)
plt.show()


# response
r = X_reco/X_true
R , std = norm.fit(r)
plt.hist(r,label='response',density=True,alpha=0.5)
# fit
xmin , xmax = plt.xlim()
x = np.linspace(xmin,xmax,100)
fit = norm.pdf(x,R,std)
plt.plot(x,fit,label='fit gaussiano')
# r calib
X_calib = X_reco / R
r_calib = X_calib/X_true
R_calib , std_calib = norm.fit(r_calib)
plt.hist(r_calib,label='response calib',density=True,alpha=0.5)
# Numerical Inversion
#R_true = 1
#X_ni = X_true * R_true
#r_ni = X_ni/X_true
#R_ni , std_ni = norm.fit(r_ni)
#plt.hist(r_ni,label='response Numerical Inversion',density=True,alpha=0.5)


plt.xlim(0,1.2)
plt.title('Response = '+str(R)+' / Reponse calib = '+str(R_calib))
plt.legend()
plt.show()
