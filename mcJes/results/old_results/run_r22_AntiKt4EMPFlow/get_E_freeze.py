# script to determine the energy freezing value for MC JES 
# runs on output of DeriveJetScales
# output similar to this (at the bottom): https://atlas-groupdata.web.cern.ch/atlas-groupdata/JetCalibTools/CalibArea-00-04-82/CalibrationFactors/MC16a_MCJES_4EMPFlow_Oct2017.config

from ROOT import TFile, TGraph, TH1D, TH1F, TF1, TGraphErrors

def get_freeze():
	fn = "online_TLA_JES.root"
	f = TFile(fn,'READ')
	neta = 90

	ll = list(range(neta))
	# the bin 89 is usually copied to a bin 90 (no idea why but for consistency follow that)
	ll.append(neta-1)

	with open(fn.split(".root")[0]+"_Emax.config","w") as txt:
		txt.write("#########\n# Starting energy for the JES freezing\n# defined as the last point where a MC fit happend (without extrapolation point for spline)\n\n")


		for i,ieta in enumerate(ll):

			g = TGraphErrors(f.Get("R_vs_Enuminv_Graph_%s" % ieta))
			npoints = g.GetN()#-1 # nose porq estaba agarrando el anteultimo

			maxx = 0
			for x in range(npoints):
				last_x = g.GetPointX(x)
				#print(last_x)
				if last_x > maxx:
					maxx = last_x

                        #print("ieta: %s, maxx: %.2f, %i" % (ieta,maxx,maxx))
                        if i==neta:
                                ieta=neta
			txt.write("EmaxJES.AntiKt4EMPFlow_Bin%s:            %i\n" % (ieta,maxx))


def main():
	get_freeze()

if __name__=="__main__":
	main()
