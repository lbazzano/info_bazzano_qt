import argparse
import os, sys
import glob
import os.path

# ***************************************************
# Interpret the arguments passed to the script from the command line.
# Use the argparse library
parser = argparse.ArgumentParser(description="""derive JES and Eta calibrations
Invocation example :
python deriveJES.py AntiKt10LCTopoTrimmedPtFrac5SmallR20 --inputPattern 'JZ*root'  -t AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets

  --> derive EtaJES correction for standard large-R jets
""",formatter_class= argparse.RawTextHelpFormatter)

parser.add_argument('--maxEvents',  type=int, default=-1)
parser.add_argument('--firstEvent',  type=int, default=0)

parser.add_argument('jetType',  type=str, help="The type of jets. Ex: 'AntiKt4LCTopo' or 'AntiKt10LCTopoTrimmedPtFrac5SmallR20' ")


parser.add_argument('--inputPattern',  type=str, nargs='?',
                    default='./JZ*root', help="A pattern to glob the input ntuples. Put it within single quotes like :  '/some/path/JZ*root' "
                    )

parser.add_argument('-c','--config',  type=str, nargs='?',
                    default='', help="The configuration file defining the calib derivation. If left blank, will be set to jetType+'.py' ")

parser.add_argument('-t','--treeName',  type=str, nargs='?',
                    default='', help="The tree name inside the input files. If left blank, will be set to 'jetType' ")

parser.add_argument('-b','--batchMode',  type=bool, nargs='?',
                    default=True, help="set batch mode (graphic display) on or off")
parser.add_argument('-r','--refConfig', type=str, nargs='?',
                    default='', help="The config file contains the calibration function parameters. Will be turned into response curve for comparison")

parser.add_argument('--postExec', type=str, nargs='?',
                    default='', help="python statement to be executed after all operations. Ex: 'a=1;print a' ")


validCalibSteps = dict( EtaJES = [ "rawresp", "rawfits", "niresp", "nifits" , "closureresp", "closurefits" ], JMS = ["rawresp", "rawfits", "niresp", "nifits" , "closureresp", "closurefits"] , Plots=["jes","jms", "jms_extract", "jms_closure", "jms_3Dviews", "jms_masswindow"], Debug=[], NoRun=[])

def interpretCalibSeq(seq):
    """ translate a string describing calib steps into a list.  """
    topSteps = seq.split('+')
    finalSteps = []
    for t in topSteps:
        subs = t.split('.')
        mainS = subs[0]
        substeps=validCalibSteps.get(mainS,None)
        if substeps is None:
            raise argparse.ArgumentTypeError("unkonwn calib step : "+mainS)

        # in all cases, add the top-level step in the sequence
        # so test like :  'JMS' in calibSteps returns true
        finalSteps+=[ mainS]

        # check substeps and add what's necessary
        if len(subs)==1:
            if substeps:
                finalSteps += [mainS+'.'+s for s in substeps]
            else:
                finalSteps += [mainS]
        if len(subs)==2:
            if not subs[1] in substeps:
                raise argparse.ArgumentTypeError("unkonwn calib subste  : "+t )
            finalSteps.append(t)
    return finalSteps


parser.add_argument('-s','--calibSteps',  type=interpretCalibSeq, nargs='?',
                    default='EtaJES+JMS+Plots', help="""What calibration derivation steps to perform. Steps are separted by '+'.
                    Ex : 'EtaJES+JMS.rawresp' means all EtaJES steps and 'rawresp' of JMS. default is 'EtaJES+JMS+Plots'
valid steps are :\n"""
                    +'\n'.join( [ '%15s  %s'%(k, v) for (k,v) in [ ('step', 'substeps') ]+validCalibSteps.items()  ]  )
                    )


args = parser.parse_args()
# ***************************************************


# ***************************************************
# load ROOT and compiled libraries
if args.batchMode : sys.argv.append('-b')
if '-t' in sys.argv: sys.argv.remove('-t') # otherwise ROOT will enable multithreading which slows down a lot the process...
import ROOT

isAnalysiBase = 'AtlasArea' in os.environ
#ROOT.gROOT.Macro("$ROOTCOREDIR/scripts/load_packages.C")  --> this causes issues with X11 on some linux desktop.
# So load libraries individually instead :
if isAnalysiBase:
    def libraryName(package): return "lib"+package+"Lib.so"
else:
    def libraryName(package): return "lib"+package+".so"

ROOT.gSystem.Load(libraryName("JES_ResponseFitter"))
ROOT.gSystem.Load(libraryName("BinnedPhaseSpace"))
ROOT.gSystem.Load(libraryName("DeriveJetScales"))

# ********************************************************************
import DeriveJetScales.CppExtensions

from DeriveJetScales.CalibrationDerivation import CalibrationDerivation
from DeriveJetScales.PyHelpers import seq

# ********************************************************************

ROOT.TH1.SetDefaultSumw2()

if __name__ == "__main__"  and args.jetType != 'deriveJES.py':

    # all input files
    inputFiles = [sorted(glob.glob(inputpattern)) for inputpattern in args.inputPattern.split(',')]
    inputFiles = [inputfile for subInputFiles in inputFiles for inputfile in subInputFiles] #flatten 'inputFiles'
    for f in inputFiles:
        print f
    if inputFiles == []:
        print " WARNING no input file found from pattern ", args.inputPattern
        print "  --> empty input files list  might cause trouble... "
        
    args.config = args.config if args.config else args.jetType+'.py'

    args.treeName = args.treeName if args.treeName else args.jetType

    # alias the actual CalibrationDerivation class
    CalibrationDerivationClass = CalibrationDerivation

    # load the configuration  : the script in args.config
    #   * can redefine CalibrationDerivationClass to a specialized one.
    #   * is expected to define the 'configure()' function which returns a fully configured CalibrationObjects instance.
    execfile( args.config )
    
    # build a TChain from the input
    c = ROOT.TChain(args.treeName)
    for f in inputFiles:
        c.Add(f)
    
    from DeriveJetScales.Plotting import JMSPlots, JESPlots
    
    "************************************************"
    print "steps ", args.calibSteps
    der = CalibrationDerivationClass()
    objs= der.initialize(c , configure, args.maxEvents, firstEvent= args.firstEvent, calibSteps=args.calibSteps)
    # add a back reference to der :
    objs.derObject = der

    # *******************************************
    # Take action according to content of args.calibSteps
    if 'JMS' in args.calibSteps or 'EtaJES' in args.calibSteps or 'JESJMS' in args.calibSteps:
        der.runDerivation()

    if 'Plots.jes' in args.calibSteps:
        jesplots = JESPlots(objs, args.refConfig)
        jesplots.drawAll()
        jesplots.dumpConstants()

    # JMS plots
    allJMS = 'Plots.jms' in args.calibSteps
    jmsToDraw = set()
    if allJMS or 'Plots.jms_extract' in args.calibSteps :
        jmsToDraw.update(['extractFile'] )
        objs.dumpConfig()                         
    if allJMS or 'Plots.jms_closure' in args.calibSteps:
        jmsToDraw.update(["mR_vs_m_closure", "mR_vs_pt_closure", "mIQR_vs_m", "mIQR_vs_pt", "calibFactors"
                          # P-A : line below is not yet working with 'very fine bins' JMS. Feel free to uncomment.
                          #"closure_mR_map", "closure_significance_map",                           
        ])
    if allJMS or 'Plots.jms_masswindow' in args.calibSteps:
        jmsToDraw.update(["mIQR_vs_pt_largeMassBins", "mR_vs_pt_largeMassBins",  "allMassWindowResp"])
    if allJMS or 'Plots.jms_3Dviews' in args.calibSteps:
        jmsToDraw.update( ["mR_vs_PtMtrue", "mRsmooth_vs_PtMtrue", "mRsmooth_vs_PtMnuminv"] )
    if 'Plots.jms_allResp' in args.calibSteps:
        jmsToDraw.update( ["allResp"] )
    if len(jmsToDraw)>0:
        jmsplots = JMSPlots( objs )
        der.readJMSFromFile( False )
        jmsplots.jmsPlots( plotsToDraw=jmsToDraw )                             
    if 'Debug' in args.calibSteps:
        print objs, objs.jmsBPS
        der.readJMSFromFile( False )

    # execute postExec
    exec(args.postExec)
