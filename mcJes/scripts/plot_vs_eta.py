import ROOT
#import sys
#import matplotlib.pyplot as plt
import numpy as np
import time
#from tqdm import tqdm
import distutils.dir_util
##############################################################################
# open
masterFolder = "../results/"



inFileFolder = "run_r22_onlineTLAparams_polBestChi_groom"
#inFileFolder = "run_r22_arthurBranch_onlineTLAparams_polBestChi_groom"
#inFileFolder = "run_r22_arthurBranch_onlineTLAparams_polBestChi"
#inFileFolder = "run_r22_onlineTLAparams_polBestChi"
#inFileFolder = "run_r22_onlineTLAparams_polBestChi_groom"
#inFileFolder = "run_r21_onlineTLAparams_polBestChi_groom"
inFileFolder = "run_r22_arthurBranch_onlineTLAparams_custom2"
#inFileFolder = "run_r22_arthurBranch_onlineTLAparams_custom_NSigmaForFit21"
#55#35#25#18#17#16#15#14#12
inFileFolder = "run_r22_arthurBranch_onlineTLAparams_custom_minPtExtrapolated25"#30,10,20,25
inFileFolder = "run_r22_arthurBranch_onlineTLAparams_custom_minEtForMeasurement32"#10,30,32,35,45,55
inFileFolder = "run_r22_arthurBranch_onlineTLAparams_custom_NSigmaForFit25_minEtForMeasurement25_minPtExtrapolated16"#13,14,16,17,20
inFileFolder = "run_r22_arthurBranch_onlineTLAparams_custom_NSigmaForFit25_minPtExtrapolated15_minEtForMeasurement22"
inFileFolder = "run_r22_arthurBranch_onlineTLAparams_custom_NSigmaForFit25_minEtForMeasurement55_minPtExtrapolated25"#20#25#30#50
#inFileFolder = "run_r22_arthurBranch_onlineTLAparams_custom_NSigmaForFit25_minEtForMeasurement55_minPtExtrapolated25_slices"
inFileFolder = "run_r22_arthurBranch_onlineTLAparams_custom_NSigmaForFit25_minEtForMeasurement55_minPtExtrapolated25_haddTest_weightTest_float"
inFileFolder = "run_r22_arthurBranch_onlineTLAparams_custom_NSigmaForFit25_minEtForMeasurement25_minPtExtrapolated15_haddTest_weightTest_float"
#inFileFolder = "run_r22_arthurBranch_onlineTLAparams_custom_NSigmaForFit25_minEtForMeasurement55_minPtExtrapolated25_haddTest"

#inFileFolder = "old_results/run_r22_test1"

inFileFolder = masterFolder+inFileFolder

#inFileName = inFileFolder+"/online_TLA_JES.root"
#inFileName = inFileFolder+"/AntiKt4EMPFlow_NoPtCut_JES.root"
inFileName = inFileFolder+"/AntiKt4EMPFlow_JES.root"


# variable
#variable = "Response"
#variable = "Closure"

ptMin = 20# 15 # 20, 25

##############################################################################
folder = inFileFolder+"/"+"r_vs_eta"
distutils.dir_util.mkpath(folder)

# eta bins
Bins        = np.arange(0, 90, 1)
Etas        = np.arange(-4.45,4.46,0.1)
xmin=-5
xmax=5
# energy points
ePoints= [150 , 200, 300 , 400, 500, 800, 1000, 1500, 2000, 3000, 4000 ]
colors = [1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 28, 32, 38, 41, 42, 45, 46, 49]
markerStyles = [24, 25, 26, 27, 28, 30, 32, 33, 22, 23, 20, 21]

unit = " GeV"

diffOne = 0

for variable in ["Response","Closure"]:
  Response = False
  Closure = False
  if variable == "Response":
    #var         = "R_vs_Enuminv"
    var         = "R_vs_Etrue"
    Response = True
    Title = "Response"
    ymin=0
    ymax=1
  if variable == "Closure":
    var         = "closure_R_vs_Etrue"
    Closure = True
    Title = "Closure"
    ymin=0.8
    ymax=1.2
  # graphs
  Graph       = var+"_Graph"          #response / Calibrated Response

  ROOT.gROOT.SetBatch(True)
  inFile = ROOT.TFile.Open(inFileName ," READ ")
  inFile.cd()
  
  ip = 0

  allGvsEta = ROOT.TMultiGraph("mg",Title)

  legend = ROOT.TLegend(0.2 ,0.15 ,0.9 ,0.25)


  for E in ePoints:

      g = ROOT.TGraphErrors(len(Bins))
      npoint=0
      for Bin,Eta in zip(Bins,Etas):
        if E/np.cosh(Eta) < ptMin: continue 
        gr     = inFile.Get(Graph+"_"+str(Bin))
        print(Graph+"_"+str(Bin))
        #R_vs_E_Graph_errp = ROOT.vector("TGraph")
        R_vs_E_Graph_errp = ROOT.TGraph()
        x, y, y_err = gr.GetX(), gr.GetY(), gr.GetEY()
        for i in range(gr.GetN()):
          R_vs_E_Graph_errp.SetPoint(i, x[i], y[i]+y_err[i])
          #R_vs_E_Graph_errp.Draw()
        try:
          y = gr.Eval(E)
          print(y,Eta,E)
          if Closure:
              diffOne += np.abs(y-1)
          err = R_vs_E_Graph_errp.Eval(E) - y
          #err = R_vs_E_Graph_errp[int(Bin)].Eval(E) - y
          g.SetPoint(npoint, Eta,  y )
          g.SetPointError(npoint, 0,  err )
          npoint+=1
        except:
          IndexError
      g.Set(npoint)
      g.SetTitle(str(E)+unit)
      g.SetFillStyle(0)
      g.SetLineWidth(2)
      g.SetLineColor(colors[ip])
      g.SetMarkerColor(colors[ip])
      #g.SetLineColor(ROOT.TColor.GetColorPalette(ip*ncolF))
      #g.SetMarkerColor(ROOT.TColor.GetColorPalette(ip*ncolF))
      g.SetMarkerStyle( markerStyles[ip] )
      legend.AddEntry(g,str(E)+unit)
      allGvsEta.Add(g)
      ip += 1

  c1 = ROOT.TCanvas( "c"+str("E"), "c"+str("E"), 1200, 1000 )
  c1.cd()
  allGvsEta.Draw("APL")
  allGvsEta.GetXaxis().SetLimits(xmin,xmax)
  allGvsEta.GetYaxis().SetRangeUser(ymin,ymax)
  allGvsEta.GetXaxis().SetTitle("#eta_{reco}")
  allGvsEta.GetYaxis().SetTitle("E response")
  legend.AddEntry(0, "p_{T} > "+str(ptMin), "")
  legend.SetNColumns(3)
  legend.SetLineWidth(0)
  legend.Draw(" same ")
  line1 = ROOT.TLine(xmin,1,xmax,1)
  latex = ROOT.TLatex()
  latex.DrawLatex(-2.3,ymin+(ymax-ymin)/4 , "#scale[0.8]{#it{ATLAS} #bf{Simulation Internal}}")
  latex.DrawLatex(-1.9,ymin+0.8*(ymax-ymin)/4 , "#scale[0.7]{#bf{#sqrt{s} = 13 TeV, Pythia Dijets}}")
  latex.Draw("same")
  line1.SetLineColor(1)
  line1.Draw("same")
  line2 = ROOT.TLine(xmin,0.99,xmax,0.99)
  line2.SetLineColor(1)
  line2.SetLineStyle(10)
  line2.Draw("same")
  line3 = ROOT.TLine(xmin,1.01,xmax,1.01)
  line3.SetLineColor(1)
  line3.SetLineStyle(10)
  line3.Draw("same")
  c1.SetGrid()
  c1.SaveAs(folder+"/Energy_vs_Eta_"+variable+"_ptmin"+str(ptMin)+".png")
  print(diffOne)
  inFile.Close()
