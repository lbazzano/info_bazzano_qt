import glob

inFileFolder = "run_r22_arthurBranch_onlineTLAparams_polBestChi_groom"
inFileFolder = "run_r22_arthurBranch_onlineTLAparams_polBestChi"
inFileFolder = "run_r22_onlineTLAparams_polBestChi"                   
#inFileFolder = "run_r22_onlineTLAparams_polBestChi_groom"             
#inFileFolder = "run_r21_onlineTLAparams_polBestChi_groom"             
inFileFolder = "run_r22_arthurBranch_onlineTLAparams_custom2"
inFileFolder = "run_r22_arthurBranch_onlineTLAparams_custom_NSigmaForFit21"
#55##35#25"#18"#17"#16"#15"#14"#12"
inFileFolder = "run_r22_arthurBranch_onlineTLAparams_custom_minPtExtrapolated25"#30,10,20,25
inFileFolder = "run_r22_arthurBranch_onlineTLAparams_custom_minEtForMeasurement32" #10,30,3235,45,55
inFileFolder = "run_r22_arthurBranch_onlineTLAparams_custom_NSigmaForFit25_minEtForMeasurement25_minPtExtrapolated16"#13,14,16,17,20
inFileFolder = "run_r22_arthurBranch_onlineTLAparams_custom_NSigmaForFit25_minPtExtrapolated15_minEtForMeasurement22"
inFileFolder = "run_r22_arthurBranch_onlineTLAparams_custom_NSigmaForFit25_minEtForMeasurement55_minPtExtrapolated25"#20#25#30#50
inFileFolder = "run_r22_arthurBranch_onlineTLAparams_custom_NSigmaForFit25_minEtForMeasurement55_minPtExtrapolated25_haddTest_weightTest_float"
inFileFolder = "run_r22_arthurBranch_onlineTLAparams_custom_NSigmaForFit25_minEtForMeasurement25_minPtExtrapolated15_haddTest_weightTest_float"
#inFileFolder = "run_r22_arthurBranch_onlineTLAparams_custom_NSigmaForFit25_minEtForMeasurement55_minPtExtrapolated25_haddTest"
source_path = "../results/"+inFileFolder

ptMin = 15

closure_image_files = [ f for f in sorted(glob.glob(source_path+'/*R_vs_Etrue/*.png'))]
response_image_files = [ f for f in sorted(glob.glob(source_path+'/*R_vs_Enuminv/*.png'))]
C_Result = [ f for f in glob.glob(source_path+'/r_vs_eta/*Closure_ptmin'+str(ptMin)+'.png')]
R_Result = [ f for f in glob.glob(source_path+'/r_vs_eta/*Response_ptmin'+str(ptMin)+'.png')]

from fpdf import FPDF
pdf = FPDF(orientation = 'L')

pdf.add_page()
pdf.image(R_Result[0],30,5,250)
pdf.add_page()
pdf.image(C_Result[0],30,5,250)

for R,C in zip(response_image_files,closure_image_files):
    print("saved a R and C")
    pdf.add_page()
    pdf.image(R,25,10,250)
    pdf.image(C,25,110,250)
    print(R,C)
print("saving pdf..."+source_path+"/Results_"+inFileFolder+".pdf")
pdf.output(source_path+"/Results_"+inFileFolder+".pdf", "F")
